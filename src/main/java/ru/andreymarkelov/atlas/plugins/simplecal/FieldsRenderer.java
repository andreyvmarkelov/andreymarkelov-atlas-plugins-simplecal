package ru.andreymarkelov.atlas.plugins.simplecal;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.impl.DateCFType;
import com.atlassian.jira.issue.customfields.impl.DateTimeCFType;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugin.userformat.FullNameUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;

public class FieldsRenderer {
    private UserManager userMgr;
    private ConstantsManager constMgr;
    private JiraAuthenticationContext context;
    private UserFormats userFormats;

    public FieldsRenderer() {
        userMgr = ComponentAccessor.getUserManager();
        constMgr = ComponentAccessor.getConstantsManager();
        context = ComponentAccessor.getJiraAuthenticationContext();
        userFormats = ComponentAccessor.getComponentOfType(UserFormats.class);
    }

    private String objToString(Object obj) {
        return (obj != null) ? obj.toString().trim() : "";
    }

    public String renderCascad(Object map) {
        if (map instanceof Map) {
            Map<String, Object> cascadValue = (Map<String, Object>) map;
            String val1 = (cascadValue.get(null) != null) ? cascadValue.get(null).toString() : "";
            String val2 = (cascadValue.get("1") != null) ? cascadValue.get("1").toString() : "";
            return val1.concat(" - ").concat(val2);
        } else {
            return map.toString();
        }
    }

    public String renderCollection(Collection<?> cfVal) {
        if (cfVal == null || cfVal.isEmpty()) {
            return "";
        }

        if (cfVal.size() == 1) {
            return objToString(cfVal.iterator().next());
        } else {
            StringBuilder sb = new StringBuilder();
            Iterator<?> iter = cfVal.iterator();
            while (iter.hasNext()) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(objToString(iter.next()));
            }
            return sb.toString();
        }
    }

    public String renderDate(Object date) {
        if (date instanceof Date) {
            DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
            DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
            return dtf.format((Date)date);
        } else {
            return date.toString();
        }
    }

    public String renderDateTime(Object date) {
        if (date instanceof Date) {
            DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
            DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_TIME_PICKER);
            return dtf.format((Date)date);
        } else {
            return date.toString();
        }
    }

    public String renderField(CustomField cf, Object cfVal) {
        if (cfVal == null) {
            return "";
        }

        String cfStrVal;
        if (cf.getCustomFieldType() instanceof UserCFType) {
            cfStrVal = renderUserRaw(cfVal);
        } else if (cf.getCustomFieldType() instanceof DateCFType) {
            cfStrVal = renderDate(cfVal);
        } else if (cf.getCustomFieldType() instanceof DateTimeCFType) {
            cfStrVal = renderDateTime(cfVal);
        } else if (cf.getCustomFieldType() instanceof CascadingSelectCFType) {
            cfStrVal = renderCascad(cfVal);
        } else {
            if (cfVal instanceof Collection<?>) {
                cfStrVal = renderCollection((Collection<?>) cfVal);
            } else {
                cfStrVal = cfVal.toString();
            }
        }
        return cfStrVal;
    }

    public String renderStatusRaw(String status) {
        Status statusObj = constMgr.getStatusObject(status);
        if (statusObj != null) {
            return statusObj.getNameTranslation(context.getI18nHelper());
        } else {
            return status;
        }
    }

    public String renderUserRaw(Object userObj) {
        if (userObj instanceof User) {
            User user = (User) userObj;
            return userFormats.forType(FullNameUserFormat.TYPE).format(user.getName(), "user_id");
        } else {
            User user = userMgr.getUserObject(userObj.toString());
            return (user != null) ? user.getDisplayName() : userObj.toString();
        }
    }

    public String renderUserRaw(String user) {
        User userObj = userMgr.getUserObject(user);
        if (userObj != null) {
            return userFormats.forType(FullNameUserFormat.TYPE).format(userObj.getName(), "user_id");
        } else {
            return user;
        }
    }
}
