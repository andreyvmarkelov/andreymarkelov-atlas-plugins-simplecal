package ru.andreymarkelov.atlas.plugins.simplecal;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.ApplicationProperties;

public class CalendarConfigClass extends JiraWebActionSupport {
    private static final long serialVersionUID = 1225110307136619908L;

    private final ApplicationProperties applicationProperties;
    private final MailRuCalCfg cfg;
    private final GroupManager grpMgr;

    private boolean isSaved = false;
    private List<String> savedGroups;
    private String[] selectedGroups = new String[0];

    public CalendarConfigClass(MailRuCalCfg cfg, GroupManager grpMgr, ApplicationProperties applicationProperties) {
        this.cfg = cfg;
        this.grpMgr = grpMgr;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public String doDefault() throws Exception {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        List<String> groups = cfg.getCalendarGroups();
        if (groups != null && !groups.isEmpty()) {
            selectedGroups = groups.toArray(new String[groups.size()]);
            savedGroups = Arrays.asList(selectedGroups);
        }

        return SUCCESS;
    }

    @Override
    @com.atlassian.jira.security.xsrf.RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        cfg.setCalendarGroups(Utils.arrayToList(selectedGroups));
        if (selectedGroups != null) {
            savedGroups = cfg.getCalendarGroups();
        }

        setSaved(true);
        return getRedirect("CalendarConfigClass!default.jspa?saved=true");
    }

    public String getBaseUrl() {
        return applicationProperties.getBaseUrl();
    }

    public Collection<Group> getGroups() {
        return grpMgr.getAllGroups();
    }

    public List<String> getSavedGroups() {
        return savedGroups;
    }

    public String[] getSelectedGroups() {
        return selectedGroups;
    }

    public boolean hasAdminPermission() {
        User user = getLoggedInUser();
        if (user == null) {
            return false;
        }
        return getPermissionManager().hasPermission(Permissions.ADMINISTER, getLoggedInUser());
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }

    public void setSavedGroups(List<String> savedGroups) {
        this.savedGroups = savedGroups;
    }

    public void setSelectedGroups(String[] selectedGroups) {
        this.selectedGroups = selectedGroups;
    }
}
