package ru.andreymarkelov.atlas.plugins.simplecal;

import java.util.LinkedHashMap;
import java.util.Map;

public class EventEntity {
    private long calId;
    private boolean allDay;
    private String color;
    private String end;
    private Map<String, String> extraFields;
    private String id;
    private String key;
    private String start;
    private String title;
    private String url;
    private boolean editable;

    public EventEntity() {}

    public void addExtraField(String key, String value) {
        if (extraFields == null) {
            extraFields = new LinkedHashMap<String, String>();
        }
        extraFields.put(key, value);
    }

    public long getCalId() {
        return calId;
    }

    public String getColor() {
        return color;
    }

    public String getEnd() {
        return end;
    }

    public Map<String, String> getExtraFields() {
        return extraFields;
    }

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getStart() {
        return start;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public void setCalId(long calId) {
        this.calId = calId;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCustomFields(Map<String, String> customFields) {
        if (extraFields == null) {
            extraFields = new LinkedHashMap<String, String>();
        }
        extraFields.putAll(customFields);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "EventEntity[calId=" + calId + ", allDay=" + allDay + ", color="
            + color + ", end=" + end + ", extraFields=" + extraFields + ", id="
            + id + ", key=" + key + ", start=" + start + ", title=" + title +
            ", url=" + url + ", editable=" + editable + "]";
    }
}
